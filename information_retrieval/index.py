from collections import defaultdict
from math import sqrt

def dot(v1, v2):
  ''' 
    computes the dot product of the two vectors 
    represented as lists
  '''

  if len(v1) != len(v2):
    raise Exception("length of vectors need to be same to perform dot product")

  return sum([ v1[i] * v2[i] for i in range(len(v1))])


commons = ['for', 'a', 'the', 'is', 'from', 'this', 'that', 'how']

def readyDoc(doc):
  '''
    return the doc as a list of single space separated words
  '''
  return ' '.join(doc.split())

def getTf(doc):
  '''
    calculate the term freqency
  '''
  d = defaultdict(int)
  for word in doc.split():
    if word in d:
      d[word] += 1
    else:
      d[word] = 1

  return d

def normalize(v):
  '''
    normalize the vector
  '''
  d = sqrt(sum([w*w for w in v]))
  return [w/float(d) for w in v]

def start(s1, s2):
  '''
    Find the cosine similarity between the two documents
  '''

  #s1 = readyDoc(s1)
  #s2 = readyDoc(s2)

  n = 2 # the number of documents
  tf1 = getTf(s1)
  tf2 = getTf(s2)

  #idfWord = defaultdict(lambda: 0, idfWord)
  idfWord = defaultdict(int)

  for w in set(tf1.keys() + tf2.keys()):
    if w in s1:
      idfWord[w] += 1
    if w in s2:
      idfWord[w] += 1

    idfWord[w] = n/float(idfWord[w])

  tf_idf1 = [tf1[w] * idfWord[w] for w in idfWord]
  tf_idf2 = [tf2[w] * idfWord[w] for w in idfWord]

  #print idfWord
  #print tf1, tf2

  #print tf_idf1, tf_idf2

  tf_idf1 = normalize(tf_idf1)
  tf_idf2 = normalize(tf_idf2)

  print dot(tf_idf2, tf_idf1)

