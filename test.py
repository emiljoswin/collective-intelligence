from math import sqrt

def get_dict(obj, p1, p2):
  # get movies rated by both p1 and p2
  sim = {}
  movies = obj[p1].keys()
 
  for m1 in obj[p2]:
    if m1 in movies:
      sim[m1] = 1

  return sim

def exp(X):
  s = sum(X)/len(X)
  return s
  

def sim_pearson(obj, p1, p2):
  sim = get_dict(obj, p1, p2)
  if len(sim) == 0: return 0

  #method 1 - using expectations - more intuitive
  # pearson corelation coefficient is the ratio between the 
  # covariance of the variables and the product of their 
  # standard deviations ie,
  # PCC = (E(XY) - E(X).E(Y)) / (sqrt(E(X^2) - E(X)^2) * sqrt(E(Y^2) - E(Y)^2))
  
  X = [ obj[p1][it] for it in sim ]
  Y = [ obj[p2][it] for it in sim ]

  XY = [ obj[p1][it] * obj[p2][it] for it in sim ]
  Xsq = [ pow(obj[p1][it], 2) for it in sim ]
  Ysq = [ pow(obj[p2][it], 2) for it in sim ]

  e_x = exp(X)
  e_y = exp(Y)
  e_xy = exp(XY)
  e_xsq = exp(Xsq)
  e_ysq = exp(Ysq)

  ans = (e_xy - e_x * e_y) / ( sqrt(e_xsq - pow(e_x, 2)) * sqrt(e_ysq - pow(e_y, 2)) )
  
  return ans
 

  # method2 - textbook method
  #Add up all the preferences
  sum1 = sum([obj[p1][it] for it in sim])
  sum2 = sum([obj[p2][it] for it in sim])
  
  #Sum up the squares
  sum1Sq = sum([pow(obj[p1][it], 2) for it in sim])
  sum2Sq = sum([pow(obj[p2][it], 2) for it in sim])

  #Sum up the products
  pSum = sum([obj[p1][it] * obj[p2][it] for it in sim])

  #Calculate Pearson Coefficient
  n = len(sim)
  num = pSum - (sum1 * sum2 / n)
  den = sqrt((sum1Sq - pow(sum1, 2)/n) * (sum2Sq - pow(sum2, 2)/n))
  if den == 0: return 0

  r = num/den
  return r

def sim_distance (obj, p1, p2):
  sim = get_dict(obj, p1, p2)

  if len(sim) == 0:
    return 0
  # find the sum of euclidian distance between the ratings they
  # have in common. The lower the distance the more similar
  # they are. Hence the inversion.

  sum_of_dist = sum([pow(obj[p1][m] - obj[p2][m], 2) for m in sim])

  return 1/(1+sum_of_dist)


def sample (data, person):
  for i in data:
    if i !=  person:
      print i, sim_pearson(data, person, i)
    
