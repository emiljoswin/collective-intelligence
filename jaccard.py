# Trying to find similarity of two sentences using the Jaccard's coefficient
# TODO  do the characterwise n-grams also and come up some 
# sample data for processing.

from nltk.util import ngrams

def get_ngrams (s1, n):
  #s1 is a list of words/characters
  return ngrams(s1, n)
  
def union (s1, s2):
  #s1, s2 are lists
  return set(s1) | set(s2)

def intersection (s1, s2):
  #s1, s2 are lists
  return set(s1) & set(s2)


def start (s1, s2, n):
  # similarity in words

  n1 = get_ngrams(s1.split(), n)
  n2 = get_ngrams(s2.split(), n)

  i = intersection(n1, n2)
  u = union(n1, n2)

  return len(i)/float(len(u))

